﻿
namespace ipz
{
    partial class FormFillingProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TITLE = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dialogue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Aqua;
            this.label1.Location = new System.Drawing.Point(115, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 75);
            this.label1.TabIndex = 0;
            this.label1.Text = "OPERATION ALLOWED\r\nMONEY BOX OPENED\r\nPUT CASH IN BOX";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TITLE
            // 
            this.TITLE.Font = new System.Drawing.Font("Showcard Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TITLE.ForeColor = System.Drawing.Color.Aqua;
            this.TITLE.Location = new System.Drawing.Point(50, 10);
            this.TITLE.Name = "TITLE";
            this.TITLE.Size = new System.Drawing.Size(400, 80);
            this.TITLE.TabIndex = 10;
            this.TITLE.Text = " WISEBANK";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Aqua;
            this.label2.Location = new System.Drawing.Point(204, 439);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "ADMIN MODE";
            // 
            // dialogue
            // 
            this.dialogue.AutoSize = true;
            this.dialogue.BackColor = System.Drawing.Color.Black;
            this.dialogue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dialogue.ForeColor = System.Drawing.Color.Aqua;
            this.dialogue.Location = new System.Drawing.Point(137, 331);
            this.dialogue.Name = "dialogue";
            this.dialogue.Size = new System.Drawing.Size(218, 20);
            this.dialogue.TabIndex = 16;
            this.dialogue.Text = "Press any key when end filling";
            this.dialogue.Click += new System.EventHandler(this.dialogue_Click);
            // 
            // FormFillingProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.dialogue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TITLE);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFillingProcess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FillingProcess";
            this.Load += new System.EventHandler(this.FormFillingProcess_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormFillingProcess_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TITLE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label dialogue;
    }
}