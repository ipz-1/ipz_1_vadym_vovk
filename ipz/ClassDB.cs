﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ipz
{
    class ClassDB
    {

        MySqlConnection connection = new MySqlConnection("server=localhost;port=3306;username=root;password=root;database=ipz;SSL MODE = None");

        public void openConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                try {
                    connection.Open();
                }
                catch(MySql.Data.MySqlClient.MySqlException)
                {
                    
                }
        }
        public void closeConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }  
        public MySqlConnection getConnection()
        {
            return connection;
        }
        public void checkConnection()
        {
            try
            {
                connection.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("No connection with server.Check connection and press OK.");
            }
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                do
                {
                    MessageBox.Show("No connection with server.Check connection and press OK.");
                    openConnection();
                }
                while (connection.State == System.Data.ConnectionState.Closed);
            }

                
        }

    }
}
